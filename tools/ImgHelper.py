'''
Created on Jun 12, 2013

@author: tim.anderson

    Module containing extra helper methods to allow Tools to manipulate the images.

'''

from re import compile
from os import listdir, remove, path
from os.path import exists
from ConfigValues import tempDirectory, fileExtention

# Deletes all of the files in the temp folder.
def CleanupTempFiles():
    if exists(tempDirectory):
        for trash in listdir(tempDirectory):
            remove(path.join(tempDirectory, trash))
        else:
            pass

#Changes the names of every image between the ignore times to "ignore"
def labelIgnoredImages(imageList, params):
    ignoreStart = params["IgnoreStart"]
    ignoreEnd = params["IgnoreEnd"]
    rejectBetween =  ignoreStart < ignoreEnd
    for image in imageList:
        if  ((rejectBetween and (ignoreStart < image[1].time() < ignoreEnd)) or
            (not rejectBetween and (ignoreStart < image[1].time() or ignoreEnd > image[1].time()))):
            image[0] = "ignore"     #Change the image name to ignore.

# sort by the datetime component of the imageList.
def sortImageList(imageList):
    imageList = sorted(imageList, key = lambda x:x[1])

def extractYearMonthDayFromFolder(foldername):
    if compile("[0-9]{8}").match(foldername) is not None:
        return int(foldername[0:4]), int(foldername[4:6]), int(foldername[6:8])
    else:
        raise Exception("Incorrectly formatted folder name.")

def createDefaultFilename(camFolder, startDate):
    filename = "-".join([camFolder, startDate.isoformat(), fileExtention])
    return filename

