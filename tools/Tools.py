'''
Created on Jun 10, 2013

@author: tim.anderson
'''
from datetime import datetime, timedelta
from os import path, listdir
from subprocess import call, Popen
from sys import platform
import ConfigValues
import ImgHelper
from Processing import CreateTimestamp
from Regex import getHourMinuteWithRegex, getRegexListFromFile, getMatchingRegexes
import Image

# creates an avi of the given images, and returns the filename.
def CreateVideoFile(params):
    filename = params["Filename"]
    fps = params["Videofps"] if params["Videofps"] != "0" else "1"
    imageNamePattern = path.join(ConfigValues.tempDirectory, ConfigValues.ImagePattern)
    args = [ConfigValues.FFMpegLocation,
            "-y",
            "-f",
            "image2",
            "-r",
            fps,
            "-i",
            imageNamePattern,
            "-c:v",
            ConfigValues.encoder,
            "-r",
            fps,
            path.join(ConfigValues.outputDirName, filename),
            ]
    __RunExe(args)

def __RunExe(arguments):
    sub = call(arguments)
    
def CreateTempDirIfMissing():
    from os.path import exists
    from os import mkdir
    if not exists(ConfigValues.tempDirectory):
        mkdir(ConfigValues.tempDirectory)

def getImagesFromServer(imageList, addTimestamps = True):
    try:
        numberOfImages = len(imageList)
        x = 0
        numberOfIgnoredImages = 0
        for image in imageList:
            if image[0] == "ignore":
                numberOfIgnoredImages += 1
                if ConfigValues.debugMode:
                    print image[0] + "was ignored"
            else:
                digits = len(str(x))
                extraZerosNeeded = 5 - digits
                filename = path.abspath(path.join(ConfigValues.tempDirectory,
                                        "".join(["Image", "0"*extraZerosNeeded, str(x), ".jpeg"])))
                imageSuccess = DownloadImage(image[0], filename, image[1], True)
                if imageSuccess == "success":
                    x += 1
    
    except RuntimeError:
        import traceback
        print("Runtime error in download," + str(traceback.extract_stack()))

def DownloadImage(inFilename, outFilename, datetime, timestampImage):
    try:
        img =  Image.open(inFilename)
        if timestampImage:
            fontImage = CreateTimestamp(datetime)
            img.paste(fontImage,(2,2))
        img.save(outFilename)
        return "success"
    except Exception:
        return "fail"

def GetImageList(params):
    camDir = path.join(params["SelectedFolder"], params["CamFolder"])
    subfolders = []
    folderList = listdir(camDir)
    for folder in folderList:
        
        fullPath = path.join(params["SelectedFolder"], params["CamFolder"], folder)
        if path.isdir(fullPath):
            subfolders.append(fullPath)
    subfolders.sort
    imageList = []
    for folder in subfolders:
        foundImages = __addImagesFromFolder(folder, params["StartDateTime"], params["EndDateTime"])
        imageList.extend(foundImages)
    return imageList

def __addImagesFromFolder(folder, startDateTime, endDateTime):
    imageList = []
    lastImageTime = datetime(year = 2000, month = 1, day = 1)   #base datetime below what's going to be used.
    capTimespan = timedelta(minutes = ConfigValues.minutesPerFrameCap)
    if folder[-8::].isdigit():
        year = int(folder[-8:-4])
        month = int(folder[-4:-2])
        day = int(folder[-2::])
        folderDate = datetime(year = year, month = month, day = day)
        # check if folder date is between the start date and the end date.
        if (startDateTime.date()) <= folderDate.date() <= endDateTime.date():
            images = [x for x in listdir(folder) if path.isfile(path.join(folder, x))]
            for image in images:
                imageHour, imageMinute = getHourMinuteWithRegex(image)
                imageDateTime = datetime(folderDate.year, folderDate.month, folderDate.day, imageHour, imageMinute)
                if(startDateTime <= imageDateTime <= endDateTime):
                    if (not ConfigValues.capFramerate) or (imageDateTime - capTimespan > lastImageTime):
                        imageList.append([path.join(folder, image), imageDateTime ])
                        lastImageTime = imageDateTime
                elif imageDateTime > endDateTime:
                    break  # stop looking through the folders if you've already passed the end time.
    return imageList


def OpenInExplorer(fileLoc):
    if platform == "win32":
        Popen(r'explorer /select, "' + path.abspath(fileLoc) + r'"')
    elif platform == "darwin":
        Popen(['open', '-R', path.abspath(fileLoc)])

def findCorrectRegex(imageList):
    getMatchingRegexes(imageList[0])

def GenerateVideo (params):
    CreateTempDirIfMissing()
    ImgHelper.CleanupTempFiles()
    getRegexListFromFile()
    print ("Creating list of files to grab...")
    imageList = GetImageList(params)
    if len(imageList) == 0:  # quit out if video would have no frames in it.
        print("Video was not generated. No images were found in the given timespan. Did you select the wrong start date?")
        return
    ImgHelper.sortImageList(imageList)
    if params["IgnoreState"] == True:
        print ("Removing ignored images...")
        ImgHelper.labelIgnoredImages(imageList, params)
    print ("Downloading the images from " + params["CamFolder"] + "...")
    getImagesFromServer(imageList, params["UseTimestamps"])
    print ("Generating video...")
    CreateVideoFile(params)
    print ("Cleaning up temp files...")
    ImgHelper.CleanupTempFiles()
    print ("Finished generating video. Video should be located at: ")
    print("Rendered_Videos\\" + params["Filename"] + ".")
#     OpenInExplorer(path.abspath(path.join(ConfigValues.outputDirName, params["Filename"])))
    
def GenerateVideoNoOutput(**params):
    ImgHelper.CleanupTempFiles()
    getRegexListFromFile()
    imageList = GetImageList(params, progressFunct = None)
    if len(imageList) != 0:
        ImgHelper.sortImageList(imageList)
        ImgHelper.labelIgnoredImages(imageList, params)
        getImagesFromServer(imageList, None)
        CreateVideoFile(params)
        ImgHelper.CleanupTempFiles()
