'''
Created on Jun 14, 2013
 
@author: tim.anderson
'''
from PIL.Image import open as imgOpen, new as newImage
from PIL.ImageFont import load_default as fontLoad
from PIL.ImageDraw import Draw as DrawOn
from PIL.ImageOps import invert as InvertColors
import datetime

textScale = 2

def Resize (imageSrc, newWidth, newHeight):
    with imgOpen(imageSrc, "r") as oldImage:
        newImage = oldImage.resize((newWidth, newHeight))
        return newImage

def CreateTimestamp(datetime):
    font = fontLoad()
    text = "".join([str(datetime.day),"/",str(datetime.month),"/",str(datetime.year)," ",str(datetime.hour),":",str(datetime.minute)])
    fontSize = font.getsize(text)
    #Create the text image.
    fontImage = newImage("RGB", (fontSize[0], fontSize[1]),"black")
    DrawOn(fontImage).text((0,0),text, font = font)
    #multiply the text scale by the text scale.
    fontImage = fontImage.resize((fontSize[0]*textScale,fontSize[1]*textScale))
    fontImage = InvertColors(fontImage)
    return fontImage
