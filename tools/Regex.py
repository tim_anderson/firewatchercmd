'''
Created on Jun 14, 2013

@author: tim.anderson

    Module of helper functions for image regexes.

'''
from re import match
from os import path
from ConfigValues import regexPath
# these regexes are tuples where the first item is the regex string,
#    the 2nd and 3rd are where the hour data start and stop, and
#    the 4th and 5th  are where the minute data start and stop.
regexList = []
readmeInfo = [  "*****Each line has the regular expression, followed by indecies for the hour and minute values*******\n",
                "*************Negative values for indecies reference from the back of the string**********************\n",
                "*****Example, -1 means the last letter in the string, while 3 means the 4th letter in the string*****\n"]
defaultRegexList = [
                  ("^[0-9]{9}$", "-9", "-7", "-7", "-5\n"),
                  ("^[a-zA-Z]+[-_][0-9]{6}[-_][0-9]{6}$", "-6", "-4", "-4", "-2\n"),
                  ("^[a-zA-Z]+[_-][0-9]{8}[a-zA-Z][0-9]{9}$", "-9", "-7", "-7", "-5")
                  ]
# Attempts to regenerate the regex lists from the file. If the file is not found, it will recreate it with default regexes.
def getRegexListFromFile():
    global regexList
    if path.exists(regexPath):
        with open(regexPath, "r") as data:
            for line in data:
                if line[0] is not r"*":
                    items = line.split()
                        # read in the regex and time locations as a tuple, and put it in the regex list.
                    regexList.append(
                                     (items[0], int(items[1]), int(items[2]), int(items[3]), int(items[4]))
                                      )
        return
    else:
        regexList = defaultRegexList
        with open(regexPath, "a") as data:
            for line in readmeInfo:
                data.write(line)
            for item in defaultRegexList:
                data.write(
                           " ".join([item[0], str(item[1]), str(item[2]), str(item[3]), str(item[4])])
                           )
        getRegexListFromFile()  # tries the method again with a valid file.

def addNewRegex(regex, hourStart, hourEnd, minuteStart, minuteEnd):
    regexList.append((regex, hourStart, hourEnd, minuteStart, minuteEnd))
    with open("regexes.txt", "a") as text:
        text.writeline(
                        " ".join([regex, str(hourStart), str(hourEnd), (str(minuteStart), str(minuteEnd))]))

# returns an (hour, day) tuple from the string name, using the first matching regex.
def getHourMinuteWithRegex(filename):
    string = path.splitext(filename)[0]
    for regex in regexList:
        if match(regex[0], string) is not None:
            return (
                    int(string[regex[1]:regex[2]]),  # hour
                    int(string[regex[3]:regex[4]])  # day
                    )
    return None  # None will be returned in error conditions where no regex is matched.

    # gets the list of all regexes that can be matched to the filename. In the majority of cases, this should return only one.
def getMatchingRegexes(filename):
    matchingRegexes = []
    string = path.splitext(filename)
    for index in range(0, len(regexList)):
        if match(regexList[index], string) is not None:
            matchingRegexes.append(index)
    return matchingRegexes
