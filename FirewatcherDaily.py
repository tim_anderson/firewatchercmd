
from sys import argv, exit
from datetime import datetime, time, timedelta
from os.path import join, isdir, dirname, abspath
from os import listdir
import sys
# 
# sys.path.append(join(abspath(dirname(__file__)),"tools"))
from tools.Tools import GenerateVideo
from tools.ConfigValues import defaultDirectory, fileExtention
def makeVideoSingle():
	params["CamFolder"] = argv[1]
	params["Filename"] = "".join([camFolder,params["StartDateTime"].date.isoformat("/"),fileExtention])
	GenerateVideo(params)
def makeVideoAll():
	for folder in listdir(defaultDirectory):
		joinedDir = join(defaultDirectory, folder)
		if isdir(joinedDir) and len(listdir(joinedDir)) > 0:
			params["CamFolder"] = folder
			params["Filename"] = "".join([folder,params["StartDateTime"].date().isoformat(),fileExtention])
			GenerateVideo(params)

camFolder = None
daysAgo = 1
if argv == None:
	print "Illegal arguments. Must supply the camera, and optionally how many days ago to make the video (defaults to yesterday)"
	print "give 'all' for the camera name to make a video for all the cameras."
	exit()
elif len(argv) < 2:
	print "Illegal arguments. Must supply the camera, and optionally how many days ago to make the video(defaults to yesterday)"
	print "give 'all' for the camera name to make a video for all the cameras."
	exit()
elif len(argv) == 2:
	daysAgo = 1
elif len(argv) == 3:
	if argv[2].isdigit():
		daysAgo = int(argv[2])
	else:
		print ("Second argument must be an interger! Use 0 for today's video, 1 for yesterday's, etc.")
		
		exit()
else:
	print "Too many arguments! give a camera folder, and optionally how many days ago to make the video."
	print "give 'all' for the camera name to make a video for all the cameras."

####### Run Script #######

now = datetime.now()
params = {}
params["SelectedFolder"] = defaultDirectory
params["StartDateTime"] = datetime(year = now.year, month = now.month, day = now.day) - timedelta(days = daysAgo)
params["EndDateTime"] = datetime(year = now.year, month = now.month, day = now.day) - timedelta(days = daysAgo-1)
params["IgnoreStart"] = time(hour = 0)
params["IgnoreEnd"] = time(hour = 0)
params["IgnoreState"] = False
params["Videofps"] = "12"
params["UseTimestamps"] = "1"

if argv[1].lower() == "all":
	makeVideoAll()
else:
	makeVideoSingle()