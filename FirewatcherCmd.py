'''
Created on Jun 3, 2013

@author: tim.anderson

'''

from tools.ConfigValues import ProgramTitle, CurrentVersion
from sys import argv, exit
from datetime import datetime, timedelta, time
from ConfigValues import fileExtension
from tools.tools import GenerateVideo

print "Not yet implemented. For the time being, use FirewatcherDaily.py"
exit()

argcount = 11

def printArgumentsRequired():
	print("1. which cam folder to use.")
	print("2. start month(1 to 12)")
	print("3. start day (1 to 31")
	print("4. start year in 20xx format")
	print("5. start hour (1 to 24)")
	print("6. how many days to run")
	print("7. how many hours to run (0 to 24)")
	print("8. ignore start hour (0 to 24)")
	print("9. ignore end hour (0 to 24)")
	print("10. video FPS (5 is slow, 30 is pretty fast)")
	print("11. timestamp the video? y/n")
	print (r"Example command: python Firewatcher.py bear 5 22 2013 9 3 23 23 06 15 y")
	print ("To not use any ignore times, set ignore start and ignore end to the same thing, such as zero.")


if argv == None:
	print "Illegal arguments. Firewatcher requires the following arguments:"
	printArgumentsRequired()
	exit()
elif argv.length >1 and argv[1].lower() == "help":
	print "FirewatcherCmd allows for easy, scriptable creation of videos from organized time-lapse image collections.")
	printArgumentsRequired()
elif argv.length < argcount:
	print ("too few arguments. call python Firewatcher.py help to see the arg list.")
	print ("Firewatcher requires "=str(argcount) +" arguments, you gave "+ str(argv.length))
	exit()
elif argv.length > argcount:
	print ("too many arguments. call python Firewatcher.py help to see the arg list.")
	print ("Firewatcher requires "=str(argcount) +" arguments, you gave "+ str(argv.length))
	exit()
else:
	params = {}
	params["SelectedFolder"] = r"\\secure\ftp\users\cams"
	params["CamFolder"] = argv[1]
	params["StartDateTime"] = datetime(year = int(argv[4]), month = int(argv[2]), day = int(argv[3]), hour = int(argv[5]))
	params["EndDateTime"] = params["StartDateTime"] + timedelta(days = int(argv[6]), hours = int(argv[7]))
	params["IgnoreStart"] = time(hour = int(argv[8]))
	params["IgnoreEnd"] = time(hour =int( argv[9]))
	params["IgnoreState"] = "True"
	params["Videofps"] = int(argv[10])
	params["UseTimestamps"] = "1" if argv[11].lower()[0] =="y" else "0" 
	params["Filename"] = "".join([argv[1], "-",argv[2],"/",argv[3],"/",argv[4],fileExtention])		
	GenerateVideo(params)